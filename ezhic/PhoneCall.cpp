#include "ezhic/PhoneCall.h"
#include "ezhic/AcceptUpdates.h"
#include <bdlt_currenttime.h>
#include <bsl_exception.h>

using namespace ezhic;

PhoneCall::PhoneCall(const bsl::string &contact, Record::CallIntention callIntention) : d_contact(contact), d_record(), d_duration(BloombergLP::bdlt::CurrentTime::now()) {
    d_record.intention(callIntention);
}

PhoneCall::~PhoneCall() {
    d_duration = BloombergLP::bdlt::CurrentTime::now() - d_duration;
    d_acceptUpdates->acceptUpdate(this);
}

bool PhoneCall::shouldCall() const {
    return d_record.intention() == Record::REGULAR_CALL || d_record.intention() == Record::CHECK_FOR_ANSWER;
}

bool PhoneCall::operator()() const {
    return shouldCall();
}


