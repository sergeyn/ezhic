#ifndef INCLUDED_CIRCUITPREDICATES
#define INCLUDED_CIRCUITPREDICATES

#include "ezhic/AcceptUpdates.h"
#include "ezhic/PhoneRecordPredicate.h"
#include "ezhic/Travellers.h"


namespace ezhic {
class PhoneCall;  // forward
class Registry;

namespace Circuitry {

using namespace Traversal;

typedef IteratorWrapper<PhoneRecords::const_iterator> RecordsIt;

inline RecordsIt iteratorForRecords(const PhoneRecords &phoneRecords)
{
    return RecordsIt(phoneRecords.begin(), phoneRecords.end());
}

template <typename Iterator, typename Predicate>
bool moreThanKMatch(Iterator it, Predicate predicate, size_t K)
{
    size_t acc;
    for (acc = 0; it && acc <= K; ++it) {
        acc += predicate(*it);
    }
    return acc > K;
};

const long ALL_RECORDS(-1);

// Considers S records:
// S == |phoneRecords| if M>=|phoneRecords| or M == ALL_RECORDS (else S == M)
// Returns: Whether more than K of last S records match the predicate
template <typename Predicate>
bool moreThanKInLastM(size_t k,
                      long m,
                      const PhoneRecords &phoneRecords,
                      Predicate predicate)
{
    if (m == -1)
        m = phoneRecords.size();
    AtMostNTraveller<RecordsIt> it(iteratorForRecords(phoneRecords), m);
    return moreThanKMatch(it, predicate, k);
}

class CheckMoreThanKInM
{
  public:
    explicit CheckMoreThanKInM(size_t k, long m = ALL_RECORDS);

    static size_t percentageToSize(size_t size, long m, unsigned percents);

  protected:
    const size_t d_k;
    const long d_m;
};

struct IsRecord
{
    explicit IsRecord(Record::Type record) : d_record(record) {}
    bool operator()(const Record &record) { return d_record == record.type(); }
    const Record::Type d_record;
};

template <typename Record::Type record>
class CheckMoreThanKEventsInM
: public CheckMoreThanKInM,
  public PhoneRecordPredicateCloneable<CheckMoreThanKEventsInM<record> >
{
  public:
    explicit CheckMoreThanKEventsInM(size_t k, long m = ALL_RECORDS)
    : CheckMoreThanKInM(k, m)
    {}
    bool operator()(const PhoneRecords &phoneRecords) const
    {
        return moreThanKInLastM(d_k, d_m, phoneRecords, IsRecord(record));
    }
};

template <typename Record::Type record>
class CheckMoreThanKPercentEventsInM
: public CheckMoreThanKInM,
  public PhoneRecordPredicateCloneable<CheckMoreThanKPercentEventsInM<record> >
{
  public:
    explicit CheckMoreThanKPercentEventsInM(size_t percents, long m = ALL_RECORDS)
    : CheckMoreThanKInM(percents, m)
    {}
    bool operator()(const PhoneRecords &phoneRecords)
    {
        size_t k = percentageToSize(phoneRecords.size(), d_m, d_k);
        return moreThanKInLastM(k, d_m, phoneRecords, IsRecord(record));
    }
};


}  // namespace Circuitry
}  // namespace ezhic
#endif
