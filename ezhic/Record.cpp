#include "ezhic/Record.h"

#include <iostream>

namespace ezhic {

Record::Type Record::type() const
{
    return d_type;
}

void Record::type(Record::Type t)
{
    d_type = t;
}

Record::Record(Record::Type type,  Record::CallIntention callIntention)
: d_type(type)
, d_intention(callIntention)
, d_recordedAtTime()
{}

bool operator==(const Record &lhs, const Record &rhs)
{
    return lhs.type() == rhs.type() && lhs.intention() == rhs.intention() &&
           lhs.recordedAtTime() == rhs.recordedAtTime();
}

bsl::ostream &operator<<(bsl::ostream &out, const Record &record)
{
    out << '{' << "\"type\" : " << record.type()
        << ", \"intention\" : " << record.intention()
        << ", \"recordedAtTime\" : " << record.recordedAtTime();
    return out << "}";
}

Record::CallIntention Record::intention() const
{
    return d_intention;
}
void Record::intention(CallIntention r)
{
    d_intention = r;
}

void Record::recordedAtTime(Datetime sTime)
{
    d_recordedAtTime = sTime;
}

Record::Datetime Record::recordedAtTime() const
{
    return d_recordedAtTime;
}

}  // namespace ezhic