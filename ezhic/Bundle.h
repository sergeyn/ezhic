#ifndef INCLUDED_BUNDLE
#define INCLUDED_BUNDLE

#include "ezhic/CallingStrategy.h"
#include "ezhic/PhoneCall.h"
#include <bsl_string.h>
#include <bsl_utility.h>
#include <bsl_vector.h>

namespace ezhic {
class AcceptUpdates;    // forward
class CallingStrategy;  // forward
class Registry;         // forward

class Bundle : public AcceptUpdatesCloneable<Bundle>
{
  public:
    Bundle();
    ~Bundle();
    Bundle(const Bundle &);
    Bundle &operator=(const Bundle &);

    // Own a COPY of the given CallingStrategy object.
    Bundle &replaceWithClone(const CallingStrategy &nextRT);

    // Own a clone of the given object implementing AcceptUpdates.
    Bundle &replaceWithClone(const AcceptUpdates &acceptor);

    Record acceptUpdate(const PhoneCall *phoneCall) const;
    Record::CallIntention nextRunIntention(const Contact &contact) const;
    Record::CallIntention nextRunIntention(PhoneRecords &phoneRecords) const;

  protected:
    CallingStrategy d_callingStrategy;
    AcceptUpdates *d_acceptor;
    bool d_isRethrow;
};

Bundle make_mbundle(bsl::ostream &loggingStream,
                    PhoneCall::duration_t timeoutThreshold,
                    bsl::pair<unsigned, unsigned> timeoutsBeforeBreak,
                    bsl::pair<unsigned, unsigned> exceptionsBeforeBreak,
                    unsigned probeAfterKEvents,
                    unsigned successBeforeMend);
}  // namespace ezhic

#endif  // EZHIC_BUNDLE_H
