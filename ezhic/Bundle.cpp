#include <algorithm>

#include "ezhic/Bundle.h"
#include "ezhic/FrequencyPredicates.h"
#include "ezhic/PhoneCall.h"
#include "ezhic/Registry.h"
#include "ezhic/StreamLogger.h"

using namespace ezhic;

Bundle::Bundle() : d_acceptor(0), d_isRethrow(false) {}

Bundle::~Bundle()
{
    if (d_acceptor)
        delete d_acceptor;
}

Bundle::Bundle(const Bundle &rhs)
{
    d_callingStrategy = rhs.d_callingStrategy;
    d_acceptor = rhs.d_acceptor ? rhs.d_acceptor->clone() : 0;
}

Bundle &Bundle::operator=(const Bundle &rhs)
{
    if (this != &rhs) {
        d_callingStrategy = rhs.d_callingStrategy;
        d_acceptor = rhs.d_acceptor ? rhs.d_acceptor->clone() : 0;
    }
    return *this;
}

Record Bundle::acceptUpdate(const PhoneCall *phoneCall) const
{
    return d_acceptor ? d_acceptor->acceptUpdate(phoneCall)
                      : AcceptUpdates::acceptUpdate(phoneCall);
}

Bundle &Bundle::replaceWithClone(const AcceptUpdates &acceptor)
{
    if (d_acceptor)
        delete d_acceptor;
    d_acceptor = acceptor.clone();
    return *this;
}

Record::CallIntention Bundle::nextRunIntention(const Contact &contact) const
{
    return nextRunIntention(ezreg::phoneRecords(contact));
}

Record::CallIntention Bundle::nextRunIntention(PhoneRecords &phoneRecords) const
{
    return d_callingStrategy(phoneRecords);
}

Bundle &Bundle::replaceWithClone(const CallingStrategy &nextRT)
{
    d_callingStrategy = nextRT;
    return *this;
}

// TODO: fix
Bundle ezhic::make_mbundle(bsl::ostream &loggingStream,
                           PhoneCall::duration_t timeoutThreshold,
                           bsl::pair<unsigned, unsigned> timeoutsBeforeBreak,
                           bsl::pair<unsigned, unsigned> exceptionsBeforeBreak,
                           unsigned probeAfterKEvents,
                           unsigned successBeforeMend)
{
    Bundle bundle;
    AcceptorsArray aca;
    aca.cloneIn(StreamLogger(loggingStream));
    bundle.replaceWithClone(aca);

    CallingStrategy intentionTagger;

    bundle.replaceWithClone(intentionTagger);
    return bundle;
}