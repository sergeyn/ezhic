#ifndef INCLUDED_CIRCUITRY
#define INCLUDED_CIRCUITRY

#include "ezhic/Record.h"

namespace ezhic {

namespace Circuitry {
class PhoneRecordPredicate
{
  public:
    virtual ~PhoneRecordPredicate() {}
    // Const as circuitry (breakers, menders, probers) should be stateless.
    virtual bool operator()(const PhoneRecords &phoneRecords) const = 0;
    virtual PhoneRecordPredicate *clone() const = 0;
};

template <typename Derived>
class PhoneRecordPredicateCloneable : public PhoneRecordPredicate
{
  public:
    virtual PhoneRecordPredicate *clone() const
    {
        return new Derived(static_cast<Derived const &>(*this));
    }
};

}  // namespace Circuitry
}  // namespace ezhic

#endif  // EZHIC_CIRCUITRY_H
