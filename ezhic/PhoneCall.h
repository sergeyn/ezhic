#ifndef INCLUDED_PHONE_CALL
#define INCLUDED_PHONE_CALL

#include "ezhic/Record.h"

namespace ezhic {

typedef bsl::string Contact;
class AcceptUpdates;

class PhoneCall
{
  public:
    typedef BloombergLP::bsls::TimeInterval duration_t;

    explicit PhoneCall(const bsl::string &contact, Record::CallIntention);
    virtual ~PhoneCall();

    const Contact &contact() const { return d_contact; }
    const Record &record() const { return d_record; }

    bool shouldCall() const;
    bool operator()() const;

    void type(Record::Type type) { d_record.type(type); }

  private:
    Contact d_contact;
    Record d_record;
    duration_t d_duration;
    // idea is the phone call updates the phoneRecords when it is destructed. Not sure I like the AcceptUpdates interface
    AcceptUpdates* d_acceptUpdates;


  private:
    PhoneCall(const PhoneCall &);
    PhoneCall &operator=(const PhoneCall &);
};

}  // namespace ezhic

#endif  // EZHIC_PHONECALL_H
