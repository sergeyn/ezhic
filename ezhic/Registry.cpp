#include "ezhic/Registry.h"
#include "ezhic/PhoneCall.h"

using namespace ezhic;

// static
Registry *Registry::s_registry(0);
const Contact Registry::DEFAULT_BUNDLE("_DEFAULT_BUNDLE@Registry");

PhoneRecords & Registry::phoneRecords(const Contact &contact)
{
    return d_circuits[contact];
}

Registry::Registry()
{
    writeBundle(DEFAULT_BUNDLE, Bundle());
}

// this method is in an awkward place, but don't want to move too much for now.
void Registry::acceptUpdate(const PhoneCall *phoneCall)
{
    d_circuits[phoneCall->contact()].push_front(phoneCall->record());
}

const Bundle &Registry::writeBundle(const Contact &contact, const Bundle &b)
{
    Bundle &target = d_bundles[contact];
    target = b;
    phoneRecords(contact).clear();
    return target;
}

Registry &Registry::instance()
{
    if (s_registry)
        return *s_registry;
    s_registry = new Registry();
    return *s_registry;
}

const Bundle &Registry::bundle(const Contact &contact)
{
    bsl::map<Contact, Bundle>::iterator it = d_bundles.find(contact);
    if (it == d_bundles.end()) {
        it = d_bundles.insert(bsl::make_pair(contact, bundle(DEFAULT_BUNDLE)))
                 .first;
    }
    return it->second;
}

namespace ezhic {
namespace ezreg {
const Bundle &getBundle(const Contact &contact)
{
    return Registry::instance().bundle(contact);
}
const Bundle &writeBundle(const Contact &contact, const Bundle &b)
{
    return Registry::instance().writeBundle(contact, b);
}
PhoneRecords & phoneRecords(const Contact &contact)
{
    return Registry::instance().phoneRecords(contact);
}
}  // namespace ezreg
}  // namespace ezhic