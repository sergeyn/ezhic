#ifndef INCLUDED_INTENTIONTAGGER
#define INCLUDED_INTENTIONTAGGER

#include "ezhic/AcceptUpdates.h"
#include "ezhic/Record.h"

#include <vector>

namespace ezhic {

class CallingStrategy
{
  public:
    // Given a set of phone records, determine the call intention for the next call.
    // Could also go with a template interface here
    virtual Record::CallIntention operator()(PhoneRecords &phoneRecords) const;
};


}  // namespace ezhic
#endif  // INCLUDED_INTENTIONTAGGER
