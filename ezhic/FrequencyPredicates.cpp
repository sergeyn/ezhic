#include "ezhic/FrequencyPredicates.h"
#include <bsl_algorithm.h>
#include <bsl_stdexcept.h>

namespace ezhic {
namespace Circuitry {

// translate percents to actual number (mind the ALL_HISTORY convention...)
size_t CheckMoreThanKInM::percentageToSize(size_t totalSize,
                                           long sizeOfInterestOrAll,
                                           unsigned percents)
{
    size_t tsize = sizeOfInterestOrAll == ezhic::Circuitry::ALL_RECORDS
                       ? totalSize
                       : bsl::min(size_t(sizeOfInterestOrAll), totalSize);
    return tsize * percents / 100;
}

// Helper predicates ======================

struct IsSuccessOrVoicemail
{
    bool operator()(const Record &record)
    {
        return record.type() == Record::SUCCESS ||
               record.type() == Record::VOICEMAIL;
    }
};

struct IsNotEvent
{
    IsNotEvent(Record::Type record) : d_record(record) {}
    bool operator()(const Record &record) { return d_record != record.type(); }
    const Record::Type d_record;
};

CheckMoreThanKInM::CheckMoreThanKInM(size_t k, long m) : d_k(k), d_m(m) {}

}  // namespace Circuitry
}  // namespace ezhic