#ifndef INCLUDED_RECORD_H
#define INCLUDED_RECORD_H

#include <bsl_iosfwd.h>
#include <bsl_list.h>
#include <bsl_string.h>
#include <bsls_timeinterval.h>
#include <bdlt_datetime.h>

namespace ezhic {

class Record
{
  public:
    typedef BloombergLP::bdlt::Datetime Datetime;

    // Should there be a common type used by PhoneCall and Record?
    enum CallIntention
    {
        REGULAR_CALL,           // 0
        CHECK_FOR_ANSWER,       // 1
        STRAIGHT_TO_VOICEMAIL,  // 2
    };

    enum Type
    {
        SUCCESS,        // 0 got a result in time
        EXCEPTION,      // 1 exception was thrown
        VOICEMAIL,      // 2 went straight to voicemail
        TIMEOUT         // 3 sentinel, not used
    };

    explicit Record(Type type = SUCCESS, CallIntention callIntention = REGULAR_CALL);
    Type type() const;
    void type(Type t);
    CallIntention intention() const;
    void intention(CallIntention r);
    Datetime recordedAtTime() const;
    void recordedAtTime(Datetime sTime);

private:
    Type d_type;
    CallIntention d_intention;
    Datetime d_recordedAtTime;
};

bool operator==(const Record &lhs, const Record &rhs);
bsl::ostream &operator<<(bsl::ostream &, const Record &);

typedef bsl::list<Record> PhoneRecords;
}  // namespace ezhic
#endif  // EZHIC_EVENT_H
