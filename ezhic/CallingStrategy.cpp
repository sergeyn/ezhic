#include "ezhic/CallingStrategy.h"
#include "ezhic/FrequencyPredicates.h"
#include <bsl_algorithm.h>

namespace ezhic {

Record::CallIntention CallingStrategy::operator()(PhoneRecords &phoneRecords) const
{
    return Record::REGULAR_CALL;
}

}  // namespace ezhic
