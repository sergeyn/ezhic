#include <ezhic/Record.h>
#include <ezhic/PhoneCall.h>
#include <ezhic/StreamLogger.h>
#include <gtest/gtest.h>
#include <bsl_sstream.h>

using namespace ezhic;

TEST(StreamLoggerTest, null_task_returns_default_event_logs_nothing)
{
    bsl::stringstream out;
    StreamLogger logger(out);
    Record record = logger.acceptUpdate(0);
    EXPECT_EQ(record, Record());
    EXPECT_EQ(out.str(), "");
}

// Ax! the difficulties of testing
//TEST(StreamLoggerTest, logs_record_from_phoneCall_and_returns_copy_of_record)
//{
//    PhoneCall phoneCall;
//    Record record;
//    record.recordedAtTime(BloombergLP::bsls::TimeInterval(4));
//    record.duration(BloombergLP::bsls::TimeInterval(0));
//    bsl::stringstream recordStream;
//    recordStream << record << '\n';
//    phoneCall.record(record);
//
//    bsl::stringstream out;
//    StreamLogger streamLogger(out);
//
//    Record outputRecord = streamLogger.acceptUpdate(&phoneCall);
//    EXPECT_EQ(outputRecord, record);
//    EXPECT_EQ(recordStream.str(), out.str());
//}
