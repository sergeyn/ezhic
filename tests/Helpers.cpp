#include "Helpers.h"

BloombergLP::bsls::TimeInterval monotonicCurrentTime() {
    static BloombergLP::bsls::TimeInterval s_interval(0);
    s_interval.addSeconds(1);
    return s_interval;
}
