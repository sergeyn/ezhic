#include "gtest/gtest.h"
#include <ezhic/FrequencyPredicates.h>

using namespace ezhic;

namespace {

struct AlwaysTruePredicate
{
    bool operator()(const Record &) { return true; }
};
}

using namespace Circuitry;

TEST(TestPercentsToSize, ZeroPercentsReturnZero)
{
    const size_t zero(0);
    EXPECT_EQ(zero, CheckMoreThanKInM::percentageToSize(10, -1, 0));
    EXPECT_EQ(zero, CheckMoreThanKInM::percentageToSize(10, 5, 0));
    EXPECT_EQ(zero, CheckMoreThanKInM::percentageToSize(10, 20, 0));
}

TEST(TestPercentsToSize, FiftyReturnsHalfOfMIfPositive)
{
    EXPECT_EQ(3u, CheckMoreThanKInM::percentageToSize(10, 7, 50));
    EXPECT_EQ(4u, CheckMoreThanKInM::percentageToSize(10, 8, 50));
    EXPECT_EQ(0u, CheckMoreThanKInM::percentageToSize(10, 0, 50));
}

TEST(TestPercentsToSize, TwentyFiveReturnsQuarterOfSizeIfMIsAll)
{
    EXPECT_EQ(2u, CheckMoreThanKInM::percentageToSize(10, ALL_RECORDS, 25));
    EXPECT_EQ(3u, CheckMoreThanKInM::percentageToSize(13, ALL_RECORDS, 25));
    EXPECT_EQ(0u, CheckMoreThanKInM::percentageToSize(1, ALL_RECORDS, 25));
}

TEST(TestPercentsToSize, TwentyReturnsFifthOfSizeIfMIsLarger)
{
    EXPECT_EQ(5u, CheckMoreThanKInM::percentageToSize(25, 99, 20));
    EXPECT_EQ(2u, CheckMoreThanKInM::percentageToSize(13, 99, 20));
    EXPECT_EQ(0u, CheckMoreThanKInM::percentageToSize(1, 99, 20));
}

TEST(TestMoreThanKInLastM, returnsFalseForEmptyTrace)
{
    PhoneRecords empty;
    //(size_t k, long m, const PhoneRecords &history, Predicate predicate)
    EXPECT_FALSE(moreThanKInLastM(0, 10, empty, AlwaysTruePredicate()));
    EXPECT_FALSE(moreThanKInLastM(0, -1, empty, AlwaysTruePredicate()));
    EXPECT_FALSE(moreThanKInLastM(1, 10, empty, AlwaysTruePredicate()));
    EXPECT_FALSE(moreThanKInLastM(1, -1, empty, AlwaysTruePredicate()));
}

TEST(TestMoreThanKInLastM, returnsFalseOnNotEnoughTrueOnEnough)
{
    PhoneRecords trace;
    trace.push_front(Record(Record::SUCCESS));
    EXPECT_TRUE(moreThanKInLastM(0, 10, trace, AlwaysTruePredicate()));
    EXPECT_FALSE(moreThanKInLastM(1, 10, trace, AlwaysTruePredicate()));
}

TEST(TestCheckMoreThanKSuccessInM, returnsFalseForEmptyTrace)
{
    PhoneRecords empty;
    EXPECT_FALSE(CheckMoreThanKEventsInM<Record::SUCCESS>(0, 10)(empty));
    EXPECT_FALSE(CheckMoreThanKEventsInM<Record::SUCCESS>(0, -1)(empty));
    EXPECT_FALSE(CheckMoreThanKEventsInM<Record::SUCCESS>(1, 10)(empty));
    EXPECT_FALSE(CheckMoreThanKEventsInM<Record::SUCCESS>(1, -1)(empty));
}

TEST(TestCheckMoreThanKSuccessInM, returnsFalseOnNotEnoughTrueOnEnough)
{
    PhoneRecords phoneRecords;
    Record record;
    record.type(Record::SUCCESS);
    phoneRecords.push_front(record);
    EXPECT_TRUE(CheckMoreThanKEventsInM<Record::SUCCESS>(0, 10)(phoneRecords));
    EXPECT_FALSE(CheckMoreThanKEventsInM<Record::SUCCESS>(1, 10)(phoneRecords));
}

// Is this required anymore? If so, Readd the code
//TEST(TestBreakAfterKFailsInM, returnsFalseOnNotEnoughTrueOnEnough)
//{
//    Record success(Record::SUCCESS);
//    Record exception(Record::EXCEPTION);
//    PhoneRecords phoneRecords;
//    phoneRecords.push_front(success);
//    phoneRecords.push_front(exception);
//
//    EXPECT_FALSE(CheckMoreThanKEventsInM<Record::SUCCESS>(1, 2)(phoneRecords));
//    EXPECT_FALSE(CheckMoreThanKEventsInM<Record::SUCCESS>(1, -1)(phoneRecords));
//
//    phoneRecords.push_front(exception);
//    EXPECT_TRUE(BreakAfterKFailsInM(1, 2)(phoneRecords));
//    EXPECT_TRUE(BreakAfterKFailsInM(1, -1)(phoneRecords));
//
//    phoneRecords.push_front(success);
//    EXPECT_FALSE(BreakAfterKFailsInM(1, 2)(phoneRecords));
//    EXPECT_TRUE(BreakAfterKFailsInM(1, 3)(phoneRecords));
//}
