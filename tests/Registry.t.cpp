#include "gtest/gtest.h"

#include <ezhic/Registry.h>

using namespace ezhic;

TEST(TestRegistry, Named_bundle_is_persistent)
{
    const Bundle &dBundle = ezreg::getBundle("foo");
    ASSERT_EQ(&dBundle, &ezreg::getBundle("foo"));
}