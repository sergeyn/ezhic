#ifndef INCLUDED_HELPERS
#define INCLUDED_HELPERS

#include <bdlt_currenttime.h>

BloombergLP::bsls::TimeInterval monotonicCurrentTime();

#endif  // EZHIC_HELPERS_H
